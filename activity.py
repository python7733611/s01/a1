# Variables
name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
rating = 99.6

print(f"I am {name} and I am {age} years old. I work as a {occupation}, and my rating for {movie} is {rating}%.")

# Operators

num1, num2, num3 = 15, 10, 4

print(num1*num2)
print(num1<num3)
print(num3+num2)
