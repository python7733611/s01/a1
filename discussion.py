# [Section] Comments
# Comments in Python are done using CTRL+/. They are denoted with a pound symbol.


# [Section] Python Syntax
# Hello World in Python
print("Hello World!") # Either a single quote or a double quote will work. However, note that each programming language has their own set of best practices; in this case, it's better to use double quotes.

# [Section] Indentation and Delimiters
# In other programming languages, indentation in code is for readability only. In Python, however, indentation is more important than that.
# In Python, indentation is used to indicate a block of code rather than curly braces.
# Similar to JavaScript, there is no need to end statements with semi-colons.

# [Section] Variables
# Variables are containers of data.
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol (=) and does not need the let/const keyword, unlike in JS.

# [Section] Naming Convention
# In Python, the term used for variable names is "identifier."
# All identifiers should begin with a letter (A-Z or a-z), dollar sign ($), or an underscore (_).
# After the first character, identifiers can have any combination of characters.
# Unlike JS, which uses camel-casing, Python uses snake-casing as defined in the PEP (Python Enhancement Proposal).
# Identifiers are case-sensitive.
age = 35
middle_initial = "C"
# Python allows the assigning of values to multiple variables in one line.
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name4)

# [Section] Data Types
# Data types convey what kind of information a variable holds. There are different data types and each has its own use.

# Here are the most commonly used data types:
# 1. String (str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers (int, float, complex) - for integers, decimals, and complex numbers respectively; Python automatically monitors what the number's class type is, so you don't need to specify if a number is an int, a float, or a complex.
num_of_days = 365 # This is an integer.
pi_approx = 3.1416 # This is a float.
complex_num = 1 + 5j # This is a complex number; letter j represents an imaginary number.

print(type(complex_num))

# 3. Boolean (bool) - for truth values
# Boolean values in Python start with uppercase letters.
isLearning = True
isDifficult = False

# [Section] Using Variables
# Just like JS, variables are used by simply calling the name of the identifier.

print("My name is " + full_name)
print(full_name + " " + secret_code)
# [Section] Terminal Output
# In Python, printing in the terminal is done by using the print() function.
# To print identifiers, concatenation using (+) can be used.
# We cannot, however, concatenate strings to a number or to other data types.
#print("My age is " + age) This will result in an error due to the absence of type coercion. "TypeError: can only concatenate str (not "int") to str"

# [Section] Typecasting
# Here are some functions that can be used in typecasting.
# 1. int() - converts value into an integer
print(int(3.15)) # This will return 3. Note that the number will not be rounded-off; the decimals will simply be erased.
# 2. float() - converts value into an float value
print(float(5)) # Returns 5.0
# 3. str() - converts value into a string
print("My age is " + str(age))

# Another way to avoid TypeErrors in printing without using Typecasting:
# f-strings
print(f"Hi, my name is {full_name}, and my age is {age}.")

# [Section] Operations
# Python has operator families that can be used to manipulate variables.

# Arithmetic Operators = perform mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7) # Note that this returns a float type, 3.0.
print(18%4)
print(2**6)

# Assignment Operators = used to assign values to operators (Note that there is no "reassignment operator" in Python; it's simply called an "assignment operator")

num1 = 4
num1 +=3
print(num1)

# Other operators here include -=, *=, /=, %=

# Comparison Operators = used to compare values and returns boolean values

print(1 == 1) # Recall that Python doesn't have type coercion, so (1 == "1") will return False. Think of == as automatically being like the strict equality operator (===) in JS. You can use typecasting to set the same data types (ex. (1 == int("1")) will return True)

# Other operators: !=, >=, <=, >, <

# Logical Operators = used to combine conditional statements
print(True or False) # Instead of ||, simply type or
print(True and False) # Instead of &&, type and
print(not False) # Known as the "not operator"
# Note that if you combine logical operators (False and True or True), the prioritized operator would be And, followed by Or, as there is a hierarchy for the operators. Still, it is better to add dividers like ((True or False) and (False and True)) to avoid confusion.